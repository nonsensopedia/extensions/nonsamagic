# NonsaMagic

Nonsensopedia-specific config hacks, should be loaded as an extension.

Adds some more options in Special:Preferences and sets a few pesky configuration settings that refuse to work correctly unless set after initializing other extensions.