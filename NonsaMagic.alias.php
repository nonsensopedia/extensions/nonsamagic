<?php
/**
 * Aliases for special pages in the NonsaMagic extension.
 */
// @codingStandardsIgnoreFile

$specialPageAliases = array();

/** Ponglish */
$specialPageAliases['en'] = [
	'Wolne_strony_Gry' => [ 'Wolne strony Gry', 'Szukaj wolnych stron Gry' ],
];
