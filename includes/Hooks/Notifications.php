<?php

namespace NonsaMagic\Hooks;

use MediaWiki\MediaWikiServices;
use RequestContext;

class Notifications {

	/**
	 * Now THIS is a masterpiece of a hook – IT DOES NOT HAVE ANY PARAMETERS!
	 * RequestContext::getMain() has never been classier!
	 *
	 * @return bool true to hide the notification bar, false otherwise
	 */
	public static function onEchoCanAbortNewMessagesAlert() : bool {
		$user = RequestContext::getMain()->getUser();
		$options = MediaWikiServices::getInstance()->getUserOptionsLookup();

		return !$options->getBoolOption( $user, Hooks::NONSA_HIDE_BLUEBAR );
	}

	/**
	 * Override Echo config.
	 *
	 * @param $notifs
	 * @param $cats
	 * @param $icons
	 */
	public static function onBeforeCreateEchoEvent( &$notifs, &$cats, &$icons ) {
		unset( $notifs["thank-you-edit"] );
		unset( $cats['thank-you-edit'] );
		unset( $cats['emailuser'] );
		unset( $cats['edit-user-talk']['no-dismiss'] );

		foreach ( array_keys( $notifs ) as $k ) {
			$notifs[$k]['section'] = 'alert';
		}
	}
}