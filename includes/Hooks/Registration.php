<?php

namespace NonsaMagic\Hooks;

class Registration {
	/**
	 * Modify Timeless' definition and fix some stubborn config settings.
	 */
	public static function onRegistration() : void {
		global $wgValidSkinNames,
			   $wgContentNamespaces,
			   $wgNamespacesToBeSearchedDefault;

		$wgValidSkinNames['timeless']['args'][0]['template'] = 'TimelessNonsaTemplate';

		// This is due to SMW being utterly dumb.
		// Just override it.
		$wgContentNamespaces = [ 0, 100, 102, 104, 106, 108, 114 ];
		$wgNamespacesToBeSearchedDefault = [];
		foreach ( $wgContentNamespaces as $ns ) {
			$wgNamespacesToBeSearchedDefault[$ns] = true;
		}
	}
}