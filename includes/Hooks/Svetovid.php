<?php

namespace NonsaMagic\Hooks;

use SvetovidAddLinksHook;
use SvetovidTextProcessing;
use Title;
use WikiPage;

class Svetovid implements SvetovidAddLinksHook {

	/**
	 * Allows Svetovid to add links in file captions.
	 */
	public function onSvetovidAddLinks(
		Title $targetTitle, WikiPage $page, array $texts, string &$text, int &$changes
	) : bool {
		if ( $page->getTitle()->getNamespace() !== NS_FILE ) {
			return true;
		}

		if ( preg_match(
			'/\|\s*[Cc]aption\s*=\s*([^\[|]+(\[\[[^|\]]+\|?.*?]])?)+/',
			$text,
			$matches
		) ) {
			$caption = $matches[0];
			$changes = SvetovidTextProcessing::addLinks( $caption, $targetTitle, $texts );
			$text = str_replace( $matches[0], $caption, $text );
		}

		return false;
	}
}