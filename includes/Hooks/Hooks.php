<?php

namespace NonsaMagic\Hooks;

use Html;
use MediaWiki\Hook\BeforePageDisplayHook;
use MediaWiki\Hook\GetDoubleUnderscoreIDsHook;
use MediaWiki\Hook\ParserFirstCallInitHook;
use MediaWiki\Preferences\Hook\GetPreferencesHook;
use MediaWiki\ResourceLoader\Hook\ResourceLoaderRegisterModulesHook;
use ResourceLoader;

/**
 * General hooks.
 */
class Hooks implements
	BeforePageDisplayHook,
	GetDoubleUnderscoreIDsHook,
	GetPreferencesHook,
	ParserFirstCallInitHook,
	ResourceLoaderRegisterModulesHook
{
	public const NONSA_HIDE_BLUEBAR = 'nonsa-show-bluebar';
	public const WSG_PREF_SIDEBAR_LINK = 'fagocytoza-wsg-sidebar';

	public function onBeforePageDisplay( $out, $skin ) : void {
		$out->addHTML( Html::rawElement(
			'div',
			[
				'id' => 'railmodule-preload',
				'style' => 'display: none'
			],
			$out->msg( 'railModule' )->parse()
		) );
	}

	public function onGetDoubleUnderscoreIDs( &$doubleUnderscoreIDs ) : bool {
		$doubleUnderscoreIDs = array_merge( $doubleUnderscoreIDs, [
			'ignoreseealso'
		] );
		return true;
	}

	public function onGetPreferences( $user, &$preferences ) {
		$preferences[self::NONSA_HIDE_BLUEBAR] = [
			'type' => 'toggle',
			'section' => 'echo/echotalkpage',
			'label-message' => 'nonsa-pref-show-bluebar',
			'help-message' => 'nonsa-pref-show-bluebar-help',
		];
		// wolne strony gry w sidebarze
		$preferences[self::WSG_PREF_SIDEBAR_LINK] = [
			'type' => 'check',
			'label-message' => 'fagocytoza-wsg-sidebar-pref',
			'section' => 'rendering/nonsa',
		];
	}

	public function onParserFirstCallInit( $parser ) {
		$parser->setHook(
			'discord',
			function () : string {
				return Html::element( 'iframe', [
					'src' => 'https://discordapp.com/widget?id=835598579404898344&theme=dark',
					'width' => 300,
					'height' => 450,
					'allowtransparency' => 'true',
					'frameborder' => 0,
					'sandbox' => 'allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts'
				] );
			}
		);
	}

	public function onResourceLoaderRegisterModules( ResourceLoader $rl ) : void {
		// unregister this crappy module, it calls an XHR on every view (!!!)
		// https://www.semantic-mediawiki.org/wiki/Help:Entity_examinations
		$rl->register( 'smw.entityexaminer', [] );
	}
}
