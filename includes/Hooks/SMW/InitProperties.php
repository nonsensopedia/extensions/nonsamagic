<?php

namespace NonsaMagic\Hooks;

use NonsaMagic\SMW\Properties;
use SMW\PropertyRegistry;

class InitProperties {

	public function __construct() {
	}

	/**
	 * SMW does not yet support the new style of hooks,
	 * so uhhh, use the static entry point instead.
	 *
	 * @param ...$params
	 *
	 * @return bool
	 */
	public static function run( ...$params ) : bool {
		$handler = new static();
		return $handler->onInitProperties( ...$params );
	}

	public function onInitProperties( PropertyRegistry $propertyRegistry ) : bool {
		$definitions = Properties::getPropertyDefinitions();

		foreach ( $definitions as $propertyId => $definition ) {
			$propertyRegistry->registerProperty(
				$propertyId,
				$definition['type'],
				$definition['label'],
				$definition['viewable'],
				$definition['annotable']
			);

			$propertyRegistry->registerPropertyAlias(
				$propertyId,
				wfMessage( $definition['alias'] )->text()
			);

			$propertyRegistry->registerPropertyAliasByMsgKey(
				$propertyId,
				$definition['alias']
			);

			$propertyRegistry->registerPropertyDescriptionByMsgKey(
				$propertyId,
				$definition['description']
			);
		}

		return true;
	}
}