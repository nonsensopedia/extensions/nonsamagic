<?php

namespace NonsaMagic\Hooks;

use Config;
use MediaWiki\MediaWikiServices;
use MediaWiki\Revision\SlotRecord;
use MediaWiki\Storage\RevisionStore;
use NonsaMagic\SMW\SeeAlsoAnnotator;
use PageProps;
use SMW\SemanticData;
use SMW\Store;
use Title;
use Wikimedia\Rdbms\ILoadBalancer;
use WikitextContent;

class BeforeDataUpdateComplete {

	private Config $mainConfig;
	private RevisionStore $revisionStore;
	private PageProps $pageProps;

	public function __construct(
		Config $mainConfig,
		RevisionStore $revisionStore,
		PageProps $pageProps
	) {
		// because f. ServiceOptions, that's why.
		$this->mainConfig = $mainConfig;
		$this->revisionStore = $revisionStore;
		$this->pageProps = $pageProps;
	}

	/**
	 * SMW does not yet support the new style of hooks,
	 * so uhhh, use the static entry point instead.
	 *
	 * @param array $params
	 */
	public static function run( ...$params ) {
		$services = MediaWikiServices::getInstance();
		$handler = new static(
			$services->getMainConfig(),
			$services->getRevisionStore(),
			$services->getPageProps()
		);
		$handler->onBeforeDataUpdateComplete( ...$params );
	}

	/**
	 * Do the dance.
	 *
	 * @param Store $store
	 * @param SemanticData $semanticData
	 */
	public function onBeforeDataUpdateComplete( Store $store, SemanticData $semanticData ) {
		$title = $semanticData->getSubject()->getTitle();
		if ( $title === null ) {
			return;
		}

		$namespaces = $this->mainConfig->get( 'NLSeeAlsoNamespaces' );
		// Ignore redirects, pages in non-content NSs and disambigs
		if ( $title->isRedirect() ||
			!in_array( $title->getNamespace(), $namespaces ) ||
			$this->pageProps->getProperties(
				$title, [ 'disambiguation', 'ignoreseealso' ]
			)
		) {
			return;
		}

		// get latest revision content
		$revision = $this->revisionStore->getRevisionByTitle( $title );
		if ( $revision === null ) {
			return;
		}
		$content = $revision->getContent( SlotRecord::MAIN );
		if ( !$content instanceof WikitextContent ) {
			return;
		}

		$linkTexts = $this->extractSeeAlsoLinks( $content->getText() );
		$links = [];
		$redlinks = [];
		foreach ( $linkTexts as $text ) {
			$link = $this->linkTextToTitle( $text );
			if ( $link === null ||
				$link->isExternal() ||
				!in_array( $link->getNamespace(), $namespaces )
			) {
				continue;
			}

			if ( $link->exists() ) {
				$links[] = $link;
			} else {
				$redlinks[] = $link;
			}
		}

		$annotator = new SeeAlsoAnnotator( $links, $redlinks );
		$annotator->addAnnotation( $semanticData );
	}

	/**
	 * @param string $text
	 *
	 * @return string[]
	 */
	private function extractSeeAlsoLinks( string $text ) : array {
		$aliases = $this->mainConfig->get( 'NLSeeAlsoAliases' );
		$aliases = array_map(
			function( $x ) {
				return preg_quote( $x, '/' );
			},
			$aliases
		);
		$regex = '/^={2,3} *(' . implode( '|', $aliases ) . ')[^=]*?={2,3}/im';

		if ( !preg_match( $regex, $text, $matches, PREG_OFFSET_CAPTURE ) ) {
			return [];
		}
		$header = $matches[0][0];
		$offset = $matches[0][1];

		// extract section text
		$seeAlso = trim( substr( $text, $offset + strlen( $header ) ) );
		if ( !$seeAlso ) {
			return [];
		}
		if ( preg_match( '/^={2,3}/m', $seeAlso, $matches, PREG_OFFSET_CAPTURE ) ) {
			$seeAlso = substr( $seeAlso, 0, $matches[0][1] );
		}

		preg_match_all( '/\[\[.*?]](?![^{]*}})/', $seeAlso, $links );
		return array_values( array_unique( $links[0] ) );
	}

	/**
	 * Transforms link's text into a link target.
	 *
	 * @param string $text
	 *
	 * @return Title|null
	 */
	private function linkTextToTitle( string $text ) : ?Title {
		if ( !preg_match( '/(?<=\[\[).*?(?=]]|\|)/', $text, $matches ) ) {
			return null;
		}
		return Title::newFromText( $matches[0] );
	}

}