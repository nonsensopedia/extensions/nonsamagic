<?php

namespace NonsaMagic\Hooks;

use MediaWiki\Hook\SidebarBeforeOutputHook;
use MediaWiki\User\UserOptionsLookup;
use MWException;
use Skin;
use SpecialPage;

class Sidebar implements SidebarBeforeOutputHook {
	private UserOptionsLookup $userOptionsLookup;

	public function __construct( UserOptionsLookup $lookup ) {
		$this->userOptionsLookup = $lookup;
	}

	/**
	 * Adds a link in the toolbox to Special:WolneStronyGry
	 *
	 * @param Skin $skin
	 * @param $sidebar
	 *
	 * @throws MWException
	 */
	public function onSidebarBeforeOutput( $skin, &$sidebar ) : void {
		if ( !$this->userOptionsLookup->getBoolOption( $skin->getUser(), Hooks::WSG_PREF_SIDEBAR_LINK ) ) {
			return;
		}

		$sidebar['TOOLBOX'][] = [
			'text' => $skin->msg( 'wolne_strony_gry' ),
			'href' => SpecialPage::getTitleFor( 'Wolne_strony_Gry' )->getFullURL()
		];
	}
}
