<?php

namespace NonsaMagic\Hooks;

use MediaWiki\Hook\ParserPreSaveTransformCompleteHook;
use NonsaMagic\Transforms;
use NamespaceInfo;

class PreSaveTransform implements ParserPreSaveTransformCompleteHook {
	/** @var Transforms\Transform[] */
	private array $transforms;

	public function __construct( NamespaceInfo $nsInfo ) {
		$this->transforms = [
			new Transforms\References( $nsInfo ),
			new Transforms\Images()
		];
	}

	public function onParserPreSaveTransformComplete( $parser, &$text ) {
		foreach ( $this->transforms as $transform ) {
			$text = $transform->apply( $parser, $text );
		}
	}
}
