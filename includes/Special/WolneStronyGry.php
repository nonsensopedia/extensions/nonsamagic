<?php

namespace NonsaMagic\Special;

use Html;
use MWException;
use OOUI;
use PermissionsError;
use SpecialPage;
use Xml;

/**
 * wolneStronyGry SpecialPage
 *
 * @file
 * @ingroup Extensions
 */
class WolneStronyGry extends SpecialPage {
	private const MODES = [
		[
			'text' => 'Tryb klasyczny',
			'prefix' => 'Strona'
		],
		[
			'text' => 'Fantasy',
			'prefix' => 'Fantasy/strona'
		],
		[
			'text' => 'RPG – łatwy',
			'prefix' => 'RPG/łatwy/strona'
		],
		[
			'text' => 'RPG – średni',
			'prefix' => 'RPG/średni/strona'
		],
		[
			'text' => 'RPG – trudny',
			'prefix' => 'RPG/trudny/strona'
		]
	];

	/**
	 * @var int
	 */
	private $modeId;

	/**
	 * @var int
	 */
	private $fromPage;

	/**
	 * @var int
	 */
	private $toPage;

	/**
	 * @var bool
	 */
	private $wasPosted;

	public function __construct() {
		parent::__construct( 'Wolne_strony_Gry', 'autoconfirmed' );
	}

	/**
	 * Show the page to the user
	 *
	 * @param string $subPage The subpage string argument (if any).
	 *
	 * @throws PermissionsError
	 * @throws \OOUI\Exception
	 * @throws MWException
	 */
	public function execute( $subPage ) {
		$out = $this->getOutput();

		$this->setHeaders();
		$out->enableOOUI();
		$out->setPageTitle( $this->msg( 'wolne_strony_gry' ) );
		$this->checkPermissions();

		$this->extractParams();
		$this->buildLayout();

		if ( $this->wasPosted ) {
			$this->showResults();
		}
	}

	private function extractParams() {
		$request = $this->getRequest();
		$this->wasPosted = $request->wasPosted();
		$this->modeId = intval( $request->getVal( 'wsgMode', '0' ) );
		$this->fromPage = intval( $request->getVal( 'wsgFrom', '0' ) );
		$this->toPage = intval( $request->getVal( 'wsgTo', '0' ) );

		if ( $this->fromPage > $this->toPage ) {
			$t = $this->fromPage;
			$this->fromPage = $this->toPage;
			$this->toPage = $t;
		}
	}

	/**
	 * @throws OOUI\Exception
	 */
	private function buildLayout() {
		$out = $this->getOutput();
		$out->addWikiMsg( 'wolne-strony-gry-intro' );
		$out->addModules( [ 'ext.nonsa.wolneStrony', 'oojs-ui' ] );
		$out->addModuleStyles( 'ext.nonsa.wolneStrony.styles' );

		$dropdownOpts = [];
		foreach ( self::MODES as $i => $v ) {
			$dropdownOpts[] = [
				'data' => $i,
				'label' => $v['text']
			];
		}

		$form = new OOUI\FieldsetLayout( [
			'items' => [
				new OOUI\FieldLayout(
					new OOUI\DropdownInputWidget( [
						'label' => 'Tryb Gry',
						'align' => 'top',
						'id' => 'wsg-mode',
						'infusable' => true,
						'options' => $dropdownOpts,
						'name' => 'wsgMode',
						'value' => $this->wasPosted ? $this->modeId : 0
					] )
				),
				new OOUI\FieldLayout(
					new OOUI\Widget( [
						'content' => new OOUI\HorizontalLayout( [
							'items' => [
								new OOUI\NumberInputWidget( [
									'step' => '1',
									'placeholder' => 'Od strony',
									'id' => 'wsg-from',
									'infusable' => true,
									'name' => 'wsgFrom',
									'value' => $this->wasPosted ? $this->fromPage : null
								] ),
								new OOUI\NumberInputWidget( [
									'step' => '1',
									'placeholder' => 'Do strony',
									'id' => 'wsg-to',
									'infusable' => true,
									'name' => 'wsgTo',
									'value' => $this->wasPosted ? $this->toPage : null
								] )
							]
						] ),
					] )
				),
				new OOUI\ButtonInputWidget( [
					'label' => "Szukaj",
					'flags' => [
						'primary',
						'progressive'
					],
					'type' => 'submit',
					'useInputTag' => true
				] )
			],
		] );

		$form = Xml::tags( 'form',
			[
				'method' => 'post'
			],
			$form
		);

		$out->addHTML( $form );
	}

	/**
	 * @throws MWException
	 */
	private function showResults() {
		$out = $this->getOutput();

		if ( $this->toPage - $this->fromPage > 1001 ) {
			$out->addHTML( $this->getErrorBox(
				'Podany zakres jest za szeroki. Maksymalna ilość stron do sprawdzenia to 1000.'
			) );
			return;
		}

		if ( $this->modeId > sizeof( self::MODES ) - 1 || $this->modeId < 0 ) {
			$out->addHTML( $this->getErrorBox( 'Idź bawić się gdzie indziej.' ) );
			return;
		}

		if ( $this->getUser()->pingLimiter( 'wolnestrony' ) ) {
			$out->addHTML( $this->getErrorBox( $this->msg( 'fagocytoza-ratelimit' ) ) );
			return;
		}

		$pages = $this->getDataFromDB();
		$out->addHTML('<h2>Wyniki</h2>');

		if ( sizeof( $pages ) === 0 ) {
			$out->addHTML( '<p>Niestety, nic nie znaleziono.</p>' );
			return;
		}

		$withBl = 0;
		$withDeleted = 0;
		$list = '';
		foreach ( $pages as $title => $val ) {
			$tags = [];
			$title = str_replace( '_', ' ', $title );

			if ( isset( $val['backlinks'] ) ) {
				$tags[] = "[[Specjalna:Zaawansowane linkujące/Gra:$title|ma linkujące [{$val['backlinks']}]]]";
				$withBl++;
			}
			if ( isset( $val['deletedrevs'] ) ) {
				$tags[] = "[[Specjalna:Odtwórz/Gra:$title|usuwany [{$val['deletedrevs']}]]]";
				$withDeleted++;
			}

			$text = "[[Gra:$title|$title]]";

			if ( sizeof( $tags ) > 0 ) {
				$text .= ' (' . implode( ', ', $tags ) . ')';
			} else {
				$text = "<u>" . $text . "</u>";
			}

			$list .= "# $text\n";
		}

		$out->addWikiMsg( 'wsg-lista-top', sizeof( $pages ), $withDeleted, $withBl );
		$out->addWikiTextAsInterface( $list );

		wfDebugLog('Fagocytoza', json_encode($pages));
	}

	private function getDataFromDB(): array {
		$prefix = self::MODES[$this->modeId]['prefix'] . '_';
		$pages = [];
		for ( $i = $this->fromPage; $i <= $this->toPage; $i++ ) {
			$x = $prefix . $i;
			$pages[$x] = [];
		}


		$dbr = wfGetDB( DB_REPLICA );
		$res = $dbr->select(
			'page',
			[ 'page_title' ],
			[
				'page_title' => array_keys( $pages ),
				'page_namespace' => 108
			],
			__METHOD__
		);

		foreach ( $res as $r ) {
			unset( $pages[$r->page_title] );
		}

		if ( sizeof( $pages ) === 0 ) {
			return [];
		}

		$keys = array_keys( $pages );
		$res = $dbr->select(
			'ab_links',
			[
				'abl_title',
				'c' => 'COUNT(abl_from)'
			],
			[
				'abl_title' => $keys,
				'abl_namespace' => 108,
				'abl_from_namespace' => 108,
				'abl_through' => 0,
			],
			__METHOD__,
			[
				'GROUP BY' => 'abl_title'
			]
		);

		foreach ( $res as $r ) {
			$pages[$r->abl_title]['backlinks'] = $r->c;
		}

		$res = $dbr->select(
			'archive',
			[
				'ar_title',
				'c' => 'COUNT(ar_id)'
			],
			[
				'ar_title' => $keys,
				'ar_namespace' => 108
			],
			__METHOD__,
			[
				'GROUP BY' => 'ar_title'
			]
		);

		foreach ( $res as $r ) {
			$pages[$r->ar_title]['deletedrevs'] = $r->c;
		}

		return $pages;
	}

	private function getErrorBox( string $text ): string {
		return Xml::tags(
				'p',
				null,
				Html::errorBox( $text )
			);
	}

	protected function getGroupName() : string {
		return 'nonsa';
	}
}
