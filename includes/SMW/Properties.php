<?php

namespace NonsaMagic\SMW;

class Properties {

	// Property IDs
	public const PROP_SEE_ALSO_LINK = '__nl_sa_link';
	public const PROP_SEE_ALSO_REDLINK = '__nl_sa_redlink';
	public const PROP_SEE_ALSO_LINK_COUNT = '__nl_sa_count';

	// Canonical labels
	private const PROP_LABEL_SEE_ALSO_LINK = 'See also link';
	private const PROP_LABEL_SEE_ALSO_REDLINK = 'See also redlink';
	private const PROP_LABEL_SEE_ALSO_LINK_COUNT = 'See also links count';

	/**
	 * @return array
	 */
	public static function getPropertyDefinitions() : array {
		return [
			self::PROP_SEE_ALSO_LINK => [
				'label' => self::PROP_LABEL_SEE_ALSO_LINK,
				'type'  => '_wpg',
				'alias' => 'nonsa-property-see-also-link',
				'description' => 'nonsa-property-see-also-link-description',
				'viewable' => true,
				'annotable' => false
			],
			self::PROP_SEE_ALSO_REDLINK => [
				'label' => self::PROP_LABEL_SEE_ALSO_REDLINK,
				'type'  => '_wpg',
				'alias' => 'nonsa-property-see-also-redlink',
				'description' => 'nonsa-property-see-also-redlink-description',
				'viewable' => true,
				'annotable' => false
			],
			self::PROP_SEE_ALSO_LINK_COUNT => [
				'label' => self::PROP_LABEL_SEE_ALSO_LINK_COUNT,
				'type'  => '_num',
				'alias' => 'nonsa-property-see-also-count',
				'description' => 'nonsa-property-see-also-count-description',
				'viewable' => true,
				'annotable' => false
			]
		];
	}
}