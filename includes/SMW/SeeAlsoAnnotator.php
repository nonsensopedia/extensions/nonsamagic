<?php

namespace NonsaMagic\SMW;

use MediaWiki\Linker\LinkTarget;
use SMW\DIProperty;
use SMW\DIWikiPage;
use SMW\SemanticData;
use SMWDINumber;

class SeeAlsoAnnotator extends PropertyAnnotator {

	/** @var LinkTarget[] */
	private array $links;
	/**
	 * @var LinkTarget[]
	 */
	private array $redlinks;

	/**
	 * SeeAlsoAnnotator constructor.
	 *
	 * @param LinkTarget[] $links
	 * @param LinkTarget[] $redlinks
	 */
	public function __construct( array $links, array $redlinks ) {
		$this->links = $links;
		$this->redlinks = $redlinks;
	}

	/**
	 * @param SemanticData $semanticData
	 */
	public function addAnnotation( SemanticData $semanticData ) : void {
		$linkProp = new DIProperty( Properties::PROP_SEE_ALSO_LINK );
		$redlinkProp = new DIProperty( Properties::PROP_SEE_ALSO_REDLINK );
		$countProp = new DIProperty( Properties::PROP_SEE_ALSO_LINK_COUNT );

		$semanticData->removeProperty( $linkProp );
		$semanticData->removeProperty( $redlinkProp );
		$semanticData->removeProperty( $countProp );

		foreach ( $this->links as $link ) {
			$semanticData->addPropertyObjectValue(
				$linkProp,
				new DIWikiPage( $link->getDBkey(), $link->getNamespace() )
			);
		}
		foreach ( $this->redlinks as $link ) {
			$semanticData->addPropertyObjectValue(
				$redlinkProp,
				new DIWikiPage( $link->getDBkey(), $link->getNamespace() )
			);
		}
		$semanticData->addPropertyObjectValue(
			$countProp,
			new SMWDINumber( count( $this->links ) )
		);
	}
}