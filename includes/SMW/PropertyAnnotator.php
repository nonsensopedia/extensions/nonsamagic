<?php

namespace NonsaMagic\SMW;

use SMW\SemanticData;

abstract class PropertyAnnotator {
	public abstract function addAnnotation( SemanticData $semanticData );
}