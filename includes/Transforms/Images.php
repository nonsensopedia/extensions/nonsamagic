<?php

namespace NonsaMagic\Transforms;

use Parser;

class Images extends Transform {

	public function apply( Parser $parser, string $wt ) : string {
		return preg_replace_callback(
			'/\[\[Plik:.*mały.*]]/',
			function ( $match ) {
				$t = preg_replace( '/\|(\d+)x\d+px\|/', '|$1px|', $match[0] );

				if ( strpos( $t, '|thumb|' ) !== false ) {
					return str_replace( '|mały|', '|', $t );
				} else {
					return str_replace( '|mały|', '|thumb|', $t );
				}
			},
			$wt
		);
	}
}