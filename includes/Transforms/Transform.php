<?php

namespace NonsaMagic\Transforms;

use Parser;

abstract class Transform {

	/**
	 * Transforms wikitext.
	 *
	 * @param Parser $parser
	 * @param string $wt
	 *
	 * @return string
	 */
	public abstract function apply( Parser $parser, string $wt ) : string;
}