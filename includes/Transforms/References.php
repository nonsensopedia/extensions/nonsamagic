<?php

namespace NonsaMagic\Transforms;

use NamespaceInfo;
use Parser;

class References extends Transform {
	private NamespaceInfo $nsInfo;

	public function __construct( NamespaceInfo $nsInfo ) {
		$this->nsInfo = $nsInfo;
	}

	public function apply( Parser $parser, string $wt ) : string {
		$page = $parser->getPage();
		if ( !$page || !$this->nsInfo->isContent( $page->getNamespace() ) ) {
			return $wt;
		}

		return preg_replace_callback(
			'/(?:^== *([^=\n]*?) *==\s*)?^<references\s*\/>/sim',
			function ( $m ) {
				if ( empty( $m[1] ) || $m[1] === 'Przypisy' ) {
					return '{{Przypisy}}';
				} else {
					return "{{Przypisy|$m[1]}}";
				}
			},
			$wt
		);
	}
}