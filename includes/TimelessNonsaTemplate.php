<?php

use MediaWiki\Skin\Timeless\TimelessTemplate;

class TimelessNonsaTemplate extends TimelessTemplate {

	/**
	 * Move the "general" pile of tools to navigation chunk.
	 *
	 * @return string
	 */
	protected function getMainNavigation() {
		$this->sidebar['tb'] = $this->pileOfTools['general'];

		return parent::getMainNavigation();
	}

	/**
	 * Return empty site-tools chunk.
	 *
	 * @param string $id
	 * @param string $headerMessage
	 * @param string $content
	 * @param array $classes
	 *
	 * @return string
	 */
	protected function getSidebarChunk( $id, $headerMessage, $content, $classes = [] ) {
		if ( $id === 'site-tools' ) {
			return '';
		}

		return parent::getSidebarChunk(
			$id,
			$headerMessage,
			$content,
			$classes
		);
	}
}