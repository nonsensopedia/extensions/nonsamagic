<?php

$magicWords = [];

/** English (English) */
$magicWords['en'] = [
	'ignoreseealso' => [ 1, '__IGNORESEEALSO__' ],
];

/** Polski (Polish) */
$magicWords['pl'] = [
	'ignoreseealso' => [ 1, '__IGNORUJZOBACZTEŻ__' ],
];